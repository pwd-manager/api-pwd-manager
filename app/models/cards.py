from app.db.database import DB
from app.schemas.cards import CardDTO

db = DB('cards')

def read_cards(textFragment: str | None):
    if textFragment:
        return db.query({'name': textFragment }, like=True)
    return db.query()

def read_card(_id: int):
    card = db.query({'id': _id})
    return card[0] if card  else None

def create_card(card: CardDTO):
    next_id = db.next_sequence('id')
    new_card = {'id': next_id, **card.__dict__}
    return db.add(new_card)

def delete_card(_id: int):
    return db.delete({'id': _id })

def update_card(_id: int, card: CardDTO):
    card = card.__dict__
    updated_card = db.update( {'id': _id}, card)
    return updated_card
