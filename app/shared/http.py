from typing import Any

class AppHTTPException(Exception):
    def __init__(
        self,
        status_code: int,
        detail: str,
        data: Any | None = None,
        count: int | None = None,
    ):
        self.status_code = status_code
        self.deatil = detail
        self.data = data
        self.count = count
        if count is None:
            del self.count


def NOT_FOUNDED(data, entity: str, many: bool = False):  # NOSONAR
    if not data:
        body = {'detail': f'Not Found {entity}', 'data': None}
        if many:
            body = body | {'count': 0, 'data': []}
        raise AppHTTPException(
            status_code=404,
            **body
        )

def NOT_CREATED(data, entity: str):  # NOSONAR
    detail = f'Not Created {entity}'
    NOT_MODIFIED(data, detail)


def NOT_UPDATED(data, entity: str):  # NOSONAR
    detail = f'Not Updated {entity}'
    NOT_MODIFIED(data, detail)


def NOT_DELETED(data, entity: str):  # NOSONAR
    detail = f'Not Deleted {entity}'
    NOT_MODIFIED(data, detail)


def NOT_MODIFIED(data, detail: str):  # NOSONAR
    if not data:
        raise AppHTTPException(
            status_code=500,
            detail=detail,
            data=None
        )
