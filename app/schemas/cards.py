from pydantic import BaseModel
from app.schemas import APIResponse


class CardDTO(BaseModel):
    url: str
    name: str
    username: str
    password: str

class Card(CardDTO):
    id: int

CardResponse = APIResponse[Card]