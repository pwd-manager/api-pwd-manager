
from typing import TypeVar, Generic, TypedDict
from pydantic import BaseModel

T = TypeVar('T')

class APIResponse(BaseModel, Generic[T]):
    detail: str
    data: T | list[T] | None
    count: int | None 
        
