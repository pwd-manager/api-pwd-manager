import os
import json
from typing import Any
from config import Settings

base_dir = os.path.dirname(os.path.abspath(__file__))
db_name = Settings().db_name


with open(f'{base_dir}/{db_name}.json', 'r') as jsonFile:
    db_file = json.load(jsonFile)


class DB():
    entity: str
    records: list[dict]

    def __init__(self, entity: str):
        self.entity = entity
        self.records = db_file.get(entity)
        
    
    def query(self, where: dict = {}, like: bool = False):
        records = self.records
        if where:
            records = self._filter_by(where, like=like)
        return records
    
    def next_sequence(self, field: str):
        seq_values = list(map(lambda x: x.get(field), self.records))
        next_seq = max(seq_values) +1
        return next_seq
    
    def add(self, new_record: dict):
        self.records.append(new_record)
        return new_record
    
    def delete(self, where: dict):
        record = self._filter_by(where)
        if record:
            self.records = self._filter_by(where, distinct=True)
            return record[0]
        return None
            

    def update(self, where: dict, new_record: dict):
        record = self.delete(where)
        primary_key = list(where.keys())[0]
        if record :
            updated_record = { 
                primary_key: record[primary_key], 
                **new_record
            }        
            return self.add(updated_record)
        return None

    def _filter_by(self, where: dict, distinct: bool = False, like: bool = False):
        records_filtered = self.records
        
        if distinct:
            func = lambda x: x.get(k) != v
        elif like:
            func = lambda x: v.lower().strip() in x.get(k).lower().strip()
        else:
            func = lambda x: x.get(k) == v
    
        for k,v in where.items():
            records_filtered = list(filter(func, records_filtered)) 
        return records_filtered if records_filtered else []