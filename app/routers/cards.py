from fastapi import APIRouter, Response
from app.shared.http import NOT_FOUNDED, NOT_CREATED, NOT_DELETED, NOT_UPDATED
from app.shared.constants import Tags
from app.models import cards as model
from app.schemas.cards import CardDTO, CardResponse

cards = APIRouter(
    prefix='/cards',
    tags=[Tags.cards]
)

@cards.get('/', response_model=CardResponse)
async def get_cards(
    textFragment: str | None = None
) -> CardResponse:
    cards = model.read_cards(textFragment) 
    NOT_FOUNDED(cards, 'Cards')
    return CardResponse(
        detail='Cards founded!',
        count=len(cards),
        data=cards,
    )

@cards.get('/{_id}', response_model=CardResponse)
async def get_card(_id: int, response: Response = None):
    card = model.read_card(_id)
    NOT_FOUNDED(card, 'Card')
    return CardResponse(
        detail='Card founded!',
        data=card
    )

@cards.post('/', response_model=CardResponse)
async def post_card(card: CardDTO) -> CardResponse:
    card = model.create_card(card)
    NOT_CREATED(card, 'Card')
    return CardResponse(
        detail='Card created!',
        data=card
    )

@cards.delete('/{_id}', response_model=CardResponse)
async def delete_card(_id: int) -> CardResponse:
    card = model.delete_card(_id)
    NOT_DELETED(card, 'Card')
    return CardResponse(
        detail='Card deleted!',
        data=card
    )

@cards.put('/{_id}', response_model=CardResponse)
async def put_card(_id: int, card: CardDTO) -> CardResponse:
    updated_card = model.update_card(_id, card)
    NOT_UPDATED(updated_card, 'Card')
    return CardResponse(
        detail='Card Updated!',
        data=updated_card
    )
