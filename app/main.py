from fastapi import FastAPI, Request, Depends
from fastapi.responses import JSONResponse
from app.shared.dependencies import get_settings
from config import APP_CONFIG, Settings
from app.shared.http import AppHTTPException
from app.routers import cards
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI(**APP_CONFIG)

origins = ['http://localhost:4200']
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


# Controlled exception
@app.exception_handler(AppHTTPException)
async def sedic_exception_handler(
    request: Request,
    exc: AppHTTPException
):
    body = exc.__dict__.copy()
    return JSONResponse(
        status_code=body.pop('status_code'),
        content=body
    )


@app.get('/', tags=['root'])
async def root(settings: Settings = Depends(get_settings)) -> dict:
    return {'api': settings.app_name}


app.include_router(cards)
