FROM python:3.11.1 AS development

WORKDIR /code

# Update pip
RUN pip install --upgrade pip

# Copy resources
COPY . /code

# Install requirements (dev)
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements-dev.txt

# Install app CLI
WORKDIR /code/cli
RUN poetry build
RUN pip install --no-cache-dir --upgrade dist/cli-0.1.0-py3-none-any.whl
ENV PATH="${PATH}:/root/.local/bin"

# Expose port
EXPOSE 80
WORKDIR /code


FROM python:3.11.1 AS production

WORKDIR /code
# Update pip
RUN pip install --upgrade pip
# Copy resources
COPY . /code
# Install requirements (prod)
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
# Expose port
EXPOSE 80