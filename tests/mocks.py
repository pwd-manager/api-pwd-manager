from fastapi import Depends

MOCKS: dict = {
    'db': Depends({}),
    'card_id': 999,
    'card_insertion': {
        'url': 'https://gitlab.com/login',
        'name': 'gitlab.com',
        'username': 'jhon_doe@gmail.com',
        'password': '_JhonDoe*99GL'
    }
}
