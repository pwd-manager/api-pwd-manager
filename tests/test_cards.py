from fastapi.testclient import TestClient
from app.main import app
from .mocks import MOCKS 
client = TestClient(app)

PATH = '/cards/'
id_card = client.get(PATH).json().get('data')[0]['id']
PATH_one = f'{PATH}{id_card}'
card_mock = MOCKS.get('card_insertion')

def test_get_cards_all():
    response = client.get(PATH)
    assert response.status_code == 200
    assert isinstance(response.json().get('data'), list)

def test_get_cards_one():
    response = client.get(PATH_one)
    assert response.status_code == 200
    assert isinstance(response.json().get('data'), dict)

def test_cards_post_one():
    response = client.post(PATH, json=card_mock)
    print(response.json())
    fields = list(response.json().get('data').keys())

    assert response.status_code == 200
    assert sorted(fields) == sorted(['id', 'url', 'name', 'username', 'password'])
    assert isinstance(response.json().get('data'), dict)

def test_cards_put_one():
    card_data = {
        'name': 'Card updated!!',
        **card_mock
    }
    response = client.put(PATH_one, json=card_data)
    assert response.status_code == 200
    assert isinstance(response.json().get('data'), dict)

    card_response = response.json().get('data')
    del card_response['id']
    assert card_response == card_data

def test_cards_delete_one():
    response = client.delete(PATH_one)
    assert response.status_code == 200
    assert isinstance(response.json().get('data'), dict)

    response = client.get(PATH_one)
    assert response.status_code == 404
