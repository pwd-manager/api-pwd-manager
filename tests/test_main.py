from fastapi.testclient import TestClient
from app.shared.dependencies import get_settings
from app.main import app
from config import Settings

def get_test_settings():
    return Settings()
app.dependency_overrides[get_settings] = get_test_settings


client = TestClient(app)


def test_root_endpoint():
    response = client.get('/')
    assert response.status_code == 200
    assert response.json() == {'api': 'Password Manager'}
