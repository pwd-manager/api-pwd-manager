<div align="center">
<img height="100" width="110" style="margin-bottom: -70px" alt="Password Manager" src="https://gitlab.com/uploads/-/system/project/avatar/33491624/fastapi.png">
<br>
<h1 align="center" style="text-decoration: none;"> Password Manager &nbsp; Rest API </h1>
</div>

<br>
<br>

**Source Code**: <a href="https://gitlab.com/pwd-manager/api-pwd-manager" target="_blank"> https://gitlab.com/pwd-manager/api-pwd-manager</a>

---

## About
This project is A Rest API service for Password Manager based on a template (https://gitlab.com/juanesquintero/fastapi-template) for FastAPI 0.88.0 framework with Python 3.11 and has:
  - Clean folder tree
  - FastAPI advanced config & features
  - Application CLI
  - Docker containerization
  - Unit Testing
  - Coverage report
  - API Documentation

With this API you will be able to read/list, create, delete and update Cards easily, through http methods.

</a>

<br>

<small>
  All commands in this project must be run on a <b>BASH</b> type terminal.
  (You can use git-bash in windows)
</small>

---

<br>

## Set up local environment
This project can be run it on your local machine or in a docker container,
so you can install just Docker or in place Pyenv.

  <b>Pyenv</b> <br>
  https://help.dreamhost.com/hc/en-us/articles/216137637 <br>
  https://realpython.com/intro-to-pyenv/ <br>
  https://github.com/pyenv/pyenv#installation

  <b>venv</b> <br>
  https://docs.python.org/es/3/library/venv.html

  <b>Docker</b> <br>
  https://www.docker.com/resources/what-container <br>
  https://www.docker.com/get-started

  <small>
    If you are in Windows it's better to just install the python specific version,
    since the pyenv is not supporting windows currently.
  </small>

---

<br>

## TechStack

<i>Python</i> 3.11.1
https://www.python.org/downloads/release/python-3102/

<i>FastAPI</i> 0.88.0
https://fastapi.tiangolo.com/

<i>Typer</i>
https://typer.tiangolo.com/

---

<br>

## SetUp

  Clone this repo

  ```console
  git clone https://gitlab.com/pwd-manager/api-pwd-manager.git
  ```

  ### Locally

  After install the local environment with pyenv use the certain python version.
  ```console
  $ pyenv shell 3.11.1
  ```

  Or just specify the python version if you are not using pyenv with the following prefix in each command.

  ```console
  $ python3.11 -m
  ```

  Upgrade your pip manager.
  ```console
  $ pip install --upgrade pip
  ```

  Create a <b><i>virtual environment</i></b> with python virtualenv
  https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/

  ```console
  $ pip install virtualenv
  $ python -m venv venv
  ```

  Activate the <b><i>virtual environment</i></b>

  ```console
  $ source venv/bin/activate
  ```

  Check the python version of the venv created above
  ```console
  (venv)$ which python
  ```

  ### Containerized

  After install [Docker Desktop](https://www.docker.com/get-started), create the images & instance the containers on the parent repo https://gitlab.com/pwd-manager/pwd-manager.

  ```console
  $ docker-compose dev
  ```
  this command will start either the UI (Angular) and this API (FastAPI).

The FastAPI app now is running on [http://localhost:8000](http://localhost:8000)

## Database
The database for this project is a mocked json file located on /app/db/pwd-manager.json


---

<br>

## Folder Structure
    📦fastapi-template
     ┣ 📂app
     ┃ ┣ 📂db
     ┃ ┣ 📂models
     ┃ ┣ 📂routers
     ┃ ┣ 📂schemas
     ┃ ┣ 📂shared
     ┃ ┣ 📜__init__.py
     ┃ ┗ 📜main.py
     ┣ 📂cli
     ┣ 📂tests
     ┣ 📜.dockerignore
     ┣ 📜.env
     ┣ 📜.gitignore
     ┣ 📜.pre-commit-config.yaml
     ┣ 📜.python-version
     ┣ 📜DEVGUIDE.md
     ┣ 📜Dockerfile
     ┣ 📜README.md
     ┣ 📜config.py
     ┣ 📜docker-compose.yml
     ┣ 📜requirements-dev.txt
     ┣ 📜requirements.txt
     ┗ 📜run.py

<br>

## Installation
<div class="termy">

```console
(venv)$ pip install requirements.txt
```

</div>

If you are in dev mode use:

<div class="termy">

```console
(venv)$ pip install requirements.txt
(venv)$ pip install requirements-dev.txt
```

</div>


### Run
  The <b><i>run.py</i></b> file has dev environment parameters to run the application.
  ```console
  python run.py
  ```


### Check it

Open your browser at <a href="http://127.0.0.1:8000/" class="external-link" target="_blank"> http://localhost:8000/ </a>

You will see the JSON response as:

```JSON
{"api": "Password Manager"}
```

<br>

## CLI
This project ineriths a custom CLI from its template (https://gitlab.com/juanesquintero/fastapi-template) to execute clear and simple commands instead long and tedious native commands.

This CLI is developed on https://typer.tiangolo.com/tutorial/package/ 0.4.0


<small>
Remember if you are running the app containerized to run these CLI commands, do it inside the docker container console (docker exec -it api_pwd-mngr_dev)
</small>


Show available commands
```console
(venv)$ pwd-mngr --help
```

Run or start api server <small>(dev env by default)</small>
```console
(venv)$ pwd-mngr start

(venv)$ pwd-mngr start --help
```

In prod mode
```
(venv)$ pwd-mngr start --env=prod
```

<br>

## Testing

  Run tests
  ```console
  (venv)$ pwd-mngr test
  ```
  <small>pytest -v tests/</small>
  <small>docker exec api_pwd-mngr_dev pwd-mngr test</small>

  Run tests & generate html report
  ```console
  (venv)$ pwd-mngr test --html
  ```
  <small>pytest -v --html=tests/report.html --self-contained-html tests/</small>

  Run coverage
  ```console
  (venv)$ pwd-mngr coverage
  ```
  <small>pytest --cov-report term-missing --cov=app tests/</small>

  Run coverage & generate html Report
  ```console
  (venv)$ pwd-mngr coverage --html
  ```
  <small>pytest --cov-report html:tests/coverage --cov-report term-missing --cov=app tests/</small>

<br>

<br>

## Pre-Commit Hooks
  The pre-commit git hooks consists on run some checks before confirm or commit your staged changes on the local repo, the <i>.pre-commit-config.yml</i> file contains the checks. <br>
  https://pre-commit.com/ <br>
  https://pypi.org/project/pre-commit/ <br>

  To set it up, you need to run this command on the root folder of the project
  ```console
  (venv)$ pre-commit
  (venv)$ pre-commit autoupdate
  ```

  It runs each time you try to commit your changes, or you can just run it manually
  ```console
  (venv)$ pre-commit run --all-files
  ```


  The following local checks are implemented:

  - Linting with PyLint
  ```console
  (venv)$ pylint *
  ```

  - Testing with PyTest
  ```console
  (venv)$ pytest -v tests/
  ```
<br>

## API Docs

### Swagger - OpenAPI

Now go to <a href="http://127.0.0.1:8000/docs" class="external-link" target="_blank"> http://localhost:8000/docs </a>

You will see the automatic interactive API documentation (provided by <a href="https://github.com/swagger-api/swagger-ui" class="external-link" target="_blank">Swagger UI</a>):

![Swagger UI](https://fastapi.tiangolo.com/img/index/index-01-swagger-ui-simple.png)

### ReDoc

And now, go to <a href="http://127.0.0.1:8000/redoc" class="external-link" target="_blank">http://localhost:8000/redoc </a>

You will see the alternative automatic documentation (provided by <a href="https://github.com/Rebilly/ReDoc" class="external-link" target="_blank">ReDoc</a>):

![ReDoc](https://fastapi.tiangolo.com/img/index/index-02-redoc-simple.png)


<br>

## Python Packages
Check the requirements.txt for prod and dev packages and the requirements-dev.txt for only dev packages.

CORE<br>
pydantic==1.10.2<br>
fastapi==0.88.0<br>
uvicorn==0.20.0<br>
python-dotenv==0.19.2<br>
cryptography==36.0.1<br>
<br>
Utils<br>
pydash==5.1.0<br>
bcrypt==4.0.1<br>
dotmap==1.3.23<br>
<br>
App CLI<br>
poetry==1.1.13<br>
poetry-core==1.0.7<br>
typer[all]==0.4.0<br>
click==8.0.3<br>

testing<br>
requests==2.27.1<br>
pytest==7.0.0<br>
pytest-html==3.1.1<br>
coverage==6.3.1<br>
pytest-cov==3.0.0<br>
<br>
linting<br>
pylint==2.5.0<br>
pylint-plugin-utils==0.6<br>
autopep8==1.5.6<br>
<br>
git dvcs<br>
enforce-git-message==1.0.1<br>
pre-commit==2.15.0<br>

## License

This project is licensed under the terms of the MIT license.
